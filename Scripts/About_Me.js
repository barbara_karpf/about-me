$( document ).ready(function() {

  $(".BenInterests").on("click", function() {
    $(".BenList").toggle();    
  });
      
  $(".AidenInterests").on("click", function() {
    $(".AidenList").toggle();
  });
 
// Change the background color of the home page
 
  var colorCount = 0;
  var colorSel = 0;
  var backgroundColor = "";
  
  $("#colorChange").on("click", function() {
    colorSel = (colorCount%5);
    //console.log("color = " + colorSel);
    switch(colorSel) {
    
    case 0:
        backgroundColor = "#6699FF";
        break;        
    case 1:
        backgroundColor = "#CAF2FF";
        break;
    case 2:
        backgroundColor = "#FFFFFF";
        break;       
    case 3:
        backgroundColor = "#FFD6AD";
        break;
    case 4:
        backgroundColor = "lightblue";
        break;    
    default:
        backgroundColor = "#FFFFFF";
    }
    $("body").css("background", backgroundColor); //edit, body must be in quotes!
    console.log("backgroundColor = " + backgroundColor);

    colorCount = colorCount + 1;
  });
 
   // has to be a better way to do this
   // spent multiple hours trying a variety of approaches, unsuccessfully
   // feel like solution was close, but am giving up for now
   $("#steelers").on("mouseover", function() {
    $(this).css("background", "white");

    document.getElementById("football").src=("Images/football.png");
  });

   $("#warriors").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("bball1").src=("Images/bball.png");
  });

   $("#playingGuitar").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("guitar").src=("Images/guitar.png");
  });

  $("#mazeRunner").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("book1").src=("Images/book.png");
  });

  $("#nikeElite").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("socks").src=("Images/socks.png");
  });

  $("#steelers" ).on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("football").src=("Images/1x1.png");
  });


  $("#warriors").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("bball1").src=("Images/1x1.png");
  }); 

  $("#playingGuitar").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("guitar").src=("Images/1x1.png");
  });

  $("#mazeRunner").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("book1").src=("Images/1x1.png");
  });

  $("#nikeElite").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("socks").src=("Images/1x1.png");
  });

   $("#dolphins").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("football2").src=("Images/football.png");
  });

  $("#dolphins").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("football2").src=("Images/1x1.png");
  });

   $("#ymca").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("bball2").src=("Images/bball.png");
  });

  $("#ymca").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("bball2").src=("Images/1x1.png");
  });

   $("#playingPiano").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("piano").src=("Images/piano.png");
  });
  $("#playingPiano").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("piano").src=("Images/1x1.png");
  });

  $("#hatchet").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("book2").src=("Images/book.png");
  });
  $("#hatchet").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("book2").src=("Images/1x1.png");
  });

  $("#gummyBears").on("mouseover", function() {
    $(this).css("background", "white");
    document.getElementById("bear").src=("Images/bear.png");
  });
  $("#gummyBears").on("mouseout", function() {
    $(this).css("background", "white");
    document.getElementById("bear").src=("Images/1x1.png");
  });

});
      